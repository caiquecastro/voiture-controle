<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [ 'customer_id', 'plate', 'model', 'year' ];

    public function owner()
    {
    	return $this->belongsTo(Customer::class, 'customer_id');
    }
}
