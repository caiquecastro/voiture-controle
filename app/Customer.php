<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [ 'name', 'email', 'document', 'address', 'phone', 'birthday' ];

    protected $dates = [ 'birthday' ];

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }
}
