<?php

namespace App\Http\Controllers;

use App\Service;
use App\Vehicle;

class DashboardController extends Controller
{
    public function index()
    {
        $vehicles = Vehicle::all();
        $services = Service::all();

        return view('home', compact('vehicles', 'services'));
    }
}
