<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
	/**
	 * Display the public index page.
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
    	return view('welcome');
    }
}
