<?php

use Illuminate\Database\Seeder;

class VehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Customer::class, 100)
            ->create()
            ->each(function ($customer) {
                /** @var \App\Customer $customer */
                $howMany = mt_rand(1, 3);
                $method = ($howMany > 1) ? 'saveMany' : 'save';
                $customer->vehicles()->$method(factory(App\Vehicle::class, $howMany)->make());
            });
    }
}
