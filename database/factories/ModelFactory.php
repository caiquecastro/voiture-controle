<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'document' => $faker->cpf,
        'address' => $faker->address,
        'phone' => $faker->cellphoneNumber,
        'birthday' => $faker->dateTimeBetween('-70 years', '-18 years'),
    ];
});

$factory->define(App\Vehicle::class, function (Faker\Generator $faker) {
   return [
        'customer_id' => function () {
            return factory(App\Customer::class)->create()->id;
        },
        'model' => $faker->company,
        'plate' => $faker->regexify('[A-Z]{3}-[0-9]{4}'),
        'year' => $faker->year
    ];
});
