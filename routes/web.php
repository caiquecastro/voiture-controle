<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PagesController@index');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'DashboardController@index');

    Route::get('jobs/{job}/xml', 'JobsController@showXml');
    Route::get('jobs/{job}/pdf', 'JobsController@showPdf');
    Route::resource('jobs', 'JobsController');

    Route::get('customers/{customer}/barcode', function (App\Customer $customer) {
        $barcodeOptions = [
            'text' => str_pad($customer->id, 12, '0', STR_PAD_LEFT),
            'barHeight' => 100,
            'barThickWidth' => 4,
            'barThinWidth' => 2,
        ];

        // No required options.
        $rendererOptions = [
            'barHeight' => 100,
        ];

        // Draw the barcode, capturing the resource:
        $renderer = \Zend\Barcode\Barcode::factory(
            'ean13',
            'image',
            $barcodeOptions,
            $rendererOptions
        );
        return new \Illuminate\Http\Response($renderer->render(), 200, ['Content-Type' => 'image/png']);
    });
    Route::resource('customers', 'CustomersController');
    Route::resource('vehicles', 'VehiclesController');
    Route::resource('services', 'ServicesController');
});