<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class PublicPagesTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function canSeeIndexPage()
    {
        $this->visit('/')
             ->see('Voiture Controlé');
    }

    /** @test */
    public function userCanGoToLogin()
    {
        $this->visit('/')
             ->click('Login')
             ->seePageIs('/login');
    }

    /** @test */
    public function userCanRegister()
    {
        $this->visit('/')
             ->click('Register')
             ->seePageIs('/register');
    }

    /** @test */
    public function registeredUserCanGoToDashboard()
    {
        $user = factory(\App\User::class)->create();

        $this->actingAs($user)
            ->visit('/')
            ->click('Home')
            ->seePageIs('/dashboard');
    }
}
