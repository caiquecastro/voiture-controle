@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block mt-1">
                    <h3 class="card-title">Novo Cliente</h3>

                    <p>
                        <a href="{{ route('customers.index') }}">Ver Clientes</a>
                    </p>

                    <div class="card-title row">
                        <form action="{{ route('customers.store') }}" method="post" class="col-md-6 offset-md-3">

                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label for="name" class="col-form-label">Name</label>
                                <input type="text" id="name" name="name" class="form-control">
                                @if ($errors->has('name'))
                                    <span class="form-control-feedback">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" id="email" name="email" class="form-control">
                                @if ($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="document">CPF</label>
                                <input type="text" id="document" name="document" class="form-control" pattern="\d{3}\.\d{3}\.\d{3}-\d{2}" required>
                                @if ($errors->has('document'))
                                    <span class="form-control-feedback">{{ $errors->first('document') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="address">Endereço</label>
                                <input type="text" id="address" name="address" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="phone">Telefone</label>
                                <input type="text" id="phone" name="phone" class="form-control" pattern="^(\(\d{2}\) \d{4,5}-\d{4})$" required>
                                <span class="help-block">(99) 99999-9999</span>
                            </div>

                            <div class="form-group">
                                <label for="birthday">Data de Nascimento</label>
                                <input type="date" id="birthday" name="birthday" class="form-control" required>
                            </div>

                            <button class="btn btn-default">Adicionar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection