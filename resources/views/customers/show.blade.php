@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-1">
                <div class="card card-block">
                    <h3 class="card-title">Cliente: {{ $customer->name }}</h3>

                    <div class="card-text">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="mt-0">Dados</h4>

                                <dl>
                                    <dt>Nome</dt>
                                    <dd>{{ $customer->name }}</dd>

                                    <dt>E-mail</dt>
                                    <dd>{{ $customer->email }}</dd>

                                    <dt>CPF:</dt>
                                    <dd>{{ $customer->document }}</dd>

                                    <dt>Endereço</dt>
                                    <dd>{{ $customer->address }}</dd>

                                    <dt>Telefone</dt>
                                    <dd>{{ $customer->phone }}</dd>

                                    <dt>Nascimento</dt>
                                    <dd>
                                        {{ $customer->birthday->age }} anos - {{ $customer->birthday->format('d/m/Y') }}
                                    </dd>

                                    <dt>Cliente desde</dt>
                                    <dd>{{ $customer->created_at->diffForHumans() }}</dd>
                                </dl>
                            </div>
                            <div class="col-md-6">
                                <img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data={{ route('customers.show', $customer) }}" alt="">
                            </div>
                        </div>

                        <h4>Veículos</h4>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Modelo</th>
                                <th>Placa</th>
                                <th>Ano</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($customer->vehicles as $vehicle)
                                <tr>
                                    <td>{{ $vehicle->model }}</td>
                                    <td>{{ $vehicle->plate }}</td>
                                    <td>{{ $vehicle->year }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <h4>Lavagens</h4>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Carro</th>
                                <th>Serviço</th>
                                <th>Valor</th>
                                <th>Data</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                        <h4>Estacionamento</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection