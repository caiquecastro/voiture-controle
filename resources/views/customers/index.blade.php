@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block mt-1">
                    <h3 class="card-title">Clientes</h3>
                    
                    <p>
                        <a href="{{ route('customers.create') }}">Novo Cliente</a>
                    </p>

                    <div class="card-text">
                        @if ($customers->count())
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Nascimento</th>
                                    <th>Cliente desde</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($customers as $customer)
                                    <tr>
                                        <td>{{ $customer->name }}</td>
                                        <td>{{ $customer->email }}</td>
                                        <td>
                                            {{ $customer->birthday->format('d/m/Y') }}
                                            ({{ $customer->birthday->age }} anos)
                                        </td>
                                        <td>{{ $customer->created_at->diffForHumans() }}</td>
                                        <td>
                                            <a href="{{ route('customers.show', $customer) }}">
                                                Ver perfil
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="p-3 text-xs-center">Nenhum cliente cadastrado ainda</p>
                        @endif

                        {{ $customers->links('pagination::bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection