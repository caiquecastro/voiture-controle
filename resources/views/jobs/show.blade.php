@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block mt-1">
                    <h3 class="card-title">Serviço Executado: {{ $job->service->name }}</h3>

                    <p>
                        <a href="{{ route('jobs.index') }}">Ver serviços executados</a>
                    </p>

                    <div class="card-text">
                        <a class="btn btn-primary mb-1" href="https://docs.oracle.com/cd/E39557_01/doc.91/e51642/img/danfe_example_90_1.gif" download>Fazer download</a>
                        <a class="btn btn-primary mb-1" href="#">Enviar para cliente</a>
                        <a class="btn btn-primary mb-1" href="/jobs/{{ $job->id }}/xml">Baixar XML</a>
                        <br>
                       <img src="https://docs.oracle.com/cd/E39557_01/doc.91/e51642/img/danfe_example_90_1.gif" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection