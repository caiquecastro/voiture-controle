@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block mt-1">
                    <h3 class="card-title">Serviços Executados</h3>

                    <p>
                        <a href="{{ route('jobs.create') }}">Executar serviço</a>
                    </p>

                    <div class="card-text">
                        @if ($jobs->count())
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Serviço</th>
                                    <th>Preço</th>
                                    <th>Veículo</th>
                                    <th>Data de execução</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($jobs as $job)
                                    <tr>
                                        <td>{{ $job->service->name }}</td>
                                        <td>R$ {{ $job->service->price }}</td>
                                        <td>{{ $job->vehicle->plate }}</td>
                                        <td>{{ $job->created_at->format('d/m/Y H:i') }}</td>
                                        <td>
                                            <a href="{{ route('jobs.show', $job) }}">
                                                Ver serviço
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="p-3 text-xs-center">Nenhum serviço cadastrado ainda</p>
                        @endif

                        {{ $jobs->links('pagination::bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection