<ul class="nav navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/customers') }}">Clientes</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/vehicles') }}">Veículos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/services') }}">Tipos de Serviços</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/jobs') }}">Serviços</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/reports') }}">Relatórios</a>
    </li>
</ul>