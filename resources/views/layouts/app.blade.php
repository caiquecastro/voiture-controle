<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {
            'csrfToken': '{{ csrf_token() }}'
        };
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-dark bg-inverse">
            <button class="navbar-toggler hidden-lg-up"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarResponsive"
                    aria-controls="navbarResponsive"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
            ></button>
            <div class="collapse navbar-toggleable-md" id="navbarResponsive">
                <a class="navbar-brand" href="{{ url('/dashboard') }}">{{ config('app.name', 'Laravel') }}</a>
            
                <!-- Left Side Of Navbar -->
                @if (Auth::check())
                    @include ('layouts.partials.main_nav')
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav float-lg-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        @include ('layouts.partials.guest_nav')
                    @else
                        @include ('layouts.partials.user_nav')
                    @endif
                </ul>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
