@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block mt-1">
                    <h3 class="card-title">Serviços</h3>

                    <p>
                        <a href="{{ route('services.create') }}">Novo Serviço</a>
                    </p>

                    <div class="card-text">
                        @if ($services->count())
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Descrição</th>
                                    <th>Preço</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($services as $service)
                                    <tr>
                                        <td>{{ $service->name }}</td>
                                        <td>R$ {{ $service->price }}</td>
                                        <td>
                                            <a href="{{ route('services.show', $service) }}">
                                                Ver serviço
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="p-3 text-xs-center">Nenhum serviço cadastrado ainda</p>
                        @endif

                        {{ $services->links('pagination::bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection