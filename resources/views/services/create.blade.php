@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block mt-1">
                    <h3 class="card-title">Novo Serviço</h3>

                    <div class="card-title row">
                        <form action="{{ route('services.store') }}" method="post" class="col-md-6 offset-md-3">

                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label for="name" class="col-form-label">Descrição</label>
                                <input type="text" id="name" name="name" class="form-control">
                                @if ($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                <label for="price">Valor</label>
                                <input type="text" id="price" name="price" class="form-control" required>
                                @if ($errors->has('plate'))
                                    <span class="help-block">{{ $errors->first('plate') }}</span>
                                @endif
                            </div>

                            <button class="btn btn-default">Adicionar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection