@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block mt-1">
                    <h3 class="card-title">Novo Veículo</h3>

                    <div class="card-title row">
                        <form action="{{ route('vehicles.store') }}" method="post" class="col-md-6 offset-md-3">

                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('customer_id') ? ' has-error' : '' }}">
                                <label for="customer_id" class="control-label">Cliente</label>
                                <select id="customer_id" name="customer_id" class="form-control">
                                    <option value="" disabled selected>Sem cliente</option>
                                    @foreach($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('model') ? ' has-danger' : '' }}">
                                <label for="model" class="col-form-label">Modelo</label>
                                <input type="text" id="model" name="model" class="form-control">
                                @if ($errors->has('model'))
                                    <span class="help-block">{{ $errors->first('model') }}</span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('plate') ? ' has-danger' : '' }}">
                                <label for="plate">Placa</label>
                                <input type="text" id="plate" name="plate" class="form-control" pattern="[A-Z]{3}-[0-9]{4}" required>
                                @if ($errors->has('plate'))
                                    <span class="help-block">{{ $errors->first('plate') }}</span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                                <label for="year">Ano</label>
                                <select name="year" id="year" class="form-control">
                                    @foreach (range(date("Y"), date("Y") - 60) as $year)
                                        <option value="{{ $year }}">{{ $year }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('year'))
                                    <span class="help-block">{{ $errors->first('year') }}</span>
                                @endif
                            </div>

                            <button class="btn btn-default">Adicionar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection