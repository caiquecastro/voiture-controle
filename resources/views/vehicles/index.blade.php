@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block mt-1">
                    <h3 class="card-title">Veículos</h3>

                    <p>
                        <a href="{{ route('vehicles.create') }}">Novo Veículo</a>
                    </p>

                    <div class="card-text">
                        @if ($vehicles->count())
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Modelo</th>
                                    <th>Placa</th>
                                    <th>Ano</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($vehicles as $vehicle)
                                    <tr>
                                        <td>
                                            @if ($vehicle->owner)
                                                <a href="{{ route('customers.show', $vehicle->owner) }}">
                                                    {{ $vehicle->owner->name }}
                                                </a>
                                            @else
                                                Sem cliente associado
                                            @endif
                                        </td>
                                        <td>{{ $vehicle->model }}</td>
                                        <td>{{ $vehicle->plate }}</td>
                                        <td>{{ $vehicle->year }}</td>
                                        <td>
                                            <a href="{{ route('vehicles.show', $vehicle) }}">
                                                Ver veículo
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="p-3 text-xs-center">Nenhum veículo cadastrado ainda</p>
                        @endif

                        {{ $vehicles->links('pagination::bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection