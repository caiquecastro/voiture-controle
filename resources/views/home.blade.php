@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-block mt-1">
                <h3 class="card-title">Dashboard</h3>

                <div class="card-text">
                    <form method="post" action="{{ url('jobs')  }}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="service_id">Serviço</label>
                            <select name="service_id" id="service_id" class="form-control">
                                @foreach ($services as $service)
                                    <option value="{{ $service->id }}">
                                        {{ $service->name }} - R$ {{ $service->price }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="service_id">Veículo</label>
                            <select name="vehicle_id" id="vehicle_id" class="form-control">
                                @foreach ($vehicles as $vehicle)
                                    <option value="{{ $vehicle->id }}">
                                        {{ $vehicle->model }} ({{ $vehicle->plate }})
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <button class="btn btn-primary">Pagar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
